import React, { Component } from 'react';
import { SafeAreaView, StyleSheet } from 'react-native';
import { Home, Main } from '../src/containers'

export default class App extends Component {
  constructor() {
    super();
    this.state = {
      started: false,
    }
  }

  isStarted = () => (this.setState({ started: true }));

  renderComponent = (ComponentToRender) => {
    return (
      <ComponentToRender isStarted={this.isStarted} />
    );
  }
    
  render() {
    const { started } = this.state;
    return (
      <SafeAreaView style={styles.container}>
       {started ? this.renderComponent(Main) : this.renderComponent(Home)}
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff'
  }
});
