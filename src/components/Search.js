import React, {Component} from 'react';
import {
  View,
  TextInput,
  StyleSheet
 } from 'react-native';
 import Icon from 'react-native-vector-icons/MaterialIcons';

export class Search extends Component {
  constructor() {
    super();
    this.state = {
      searchTerm: '',
    }
  }

  handleTextChange = (searchTerm) => {
    this.props.onTextChange(searchTerm);
    this.setState({ searchTerm });
  }
  
  render() {
    const { searchTerm } = this.state;
    const { style } = this.props;
    return(
      <View style={[styles.searchContainer, style]}>
        <Icon 
          style={styles.searchIcon}
          size={20}
          name='search' 
          />
        <TextInput 
          style={styles.searchBox}
          value={searchTerm}
          onChangeText={this.handleTextChange}
          />
      </View>
    );
  }
};

const styles = StyleSheet.create({
  searchContainer: {
    alignItems: 'stretch',
    flexDirection: 'row',
    height: 40,
    backgroundColor: '#fff',
    borderWidth: 0.5,
    borderColor: '#C0C0C0',
    borderRadius: 40,
    shadowColor: '#D3D3D3',
    shadowOpacity: 0.2,
    shadowOffset: { width: -2, height: 2 },
    shadowRadius: 0,
  },
  searchBox: {
    height: 40,
    marginLeft: 'auto',
    marginRight: 'auto',
    color: 'black',
    flex: 1,
    paddingLeft: 5
  },
  searchIcon: {
    marginTop: 10,
    marginLeft: 5,
    alignItems: 'flex-start'
  }
})