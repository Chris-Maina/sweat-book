import React from 'react';
import { 
  Text,
  StyleSheet
 } from 'react-native';

export const SText = (props)=> {
  const { textStyles, text } = props;
  return(
    <Text style={[styles.sweatBookTxt, textStyles ]}>{text}</Text>
  );
}

 const styles = StyleSheet.create({
   sweatBookTxt: {
    color: 'black',
    fontSize: 24,
   }
 });
