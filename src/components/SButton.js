import React from 'react';
import { TouchableOpacity, Text, StyleSheet } from 'react-native';

export const SButton = props => {
  const { buttonText, buttonStyles, onPress, additionalTextStyles } = props;
  return (
    <TouchableOpacity style={[styles.button, buttonStyles]} onPress={onPress} >
      <Text style={[styles.buttonText, additionalTextStyles]}>{buttonText}</Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  button: {
    borderWidth: 0.5,
    borderColor: 'black',
    borderRadius: 5,
    padding: 10
  },
  buttonText: {
    color: 'black',
    fontSize: 24
  }
});
