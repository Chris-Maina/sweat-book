import React from 'react';
import { View, StyleSheet } from 'react-native';

export const Topbar = (props) => {
  const {style, children} = props;
  return(
    <View style={[styles.container, style]}>
      {children}
    </View>
  )
};

const styles = StyleSheet.create({
  container: {
    padding: 10,
  }
});
