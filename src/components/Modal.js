import React from 'react';
import { Modal } from 'react-native';
import { ExerciseList } from './index';

export const SweatModal = props => (
  <Modal 
    animationType="slide" 
    transparent={false} 
    visible={props.visibility}
  >
    <ExerciseList {...props} />
  </Modal>
)
