import React, { Component } from 'react';
import {
  View,
  FlatList,
  StyleSheet,
  TouchableOpacity
 } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Button from 'apsl-react-native-button';
import { SText, Topbar, Search, } from './index';
import { fuzzySearch } from '../services/fuzzySearch';

export class ExerciseList extends Component {
  constructor() {
    super();
    this.state = {
      foundExercises: []
    };
  }

  handleTextChange = searchText => {
    const { exercises } = this.props;
    const foundExercises = fuzzySearch(searchText, exercises, 'name');
    this.setState({ foundExercises });
  };

  handleExercisePress = (exercise) => {
    const { closeModal, addExercise } = this.props;
    addExercise(exercise);
    closeModal();
  }

  renderEmptyComponent = () => (
    <View style={styles.emptyContainer}>
      <SText textStyles={styles.emptyText} text="No exercises at the moment" />
    </View>
  );

  render() {
    const { closeModal } = this.props;
    const { foundExercises } = this.state;
    return (
      <View>
        <LinearGradient
          colors={['#0dd91c', '#81f66f']}
          style={styles.container}
        >
          <Topbar style={styles.topbar}>
            <Search
              onTextChange={this.handleTextChange}
              style={styles.searchBox}
              value="Enter search text"
            />
            <Button style={styles.clearButton} onPress={closeModal}>
              <Icon name="clear" size={30} color="white" />
            </Button>
          </Topbar>
        </LinearGradient>
        <FlatList
          data={foundExercises}
          ListEmptyComponent={this.renderEmptyComponent}
          renderItem={({ item }) => (
            <TouchableOpacity onPress={() => this.handleExercisePress(item)}>
              <View style={styles.listItem}>
                <SText style={styles.listItemText} text={item.name} />
              </View>
            </TouchableOpacity>
          )}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  emptyContainer: {
    flex: 1,
    alignItems: 'center'
  },
  emptyText: {
    fontSize: 18
  },
  container: {
    justifyContent: 'center',
    marginTop: 50
  },
  topbar: {
    padding: 5,
    flexDirection: 'row'
  },
  searchBox: {
    flex: 7,
    width: 70
  },
  clearButton: {
    flex: 1,
    width: 30,
    borderStyle: null,
    borderWidth: 0
  },
  listItem: {
    padding: 10,
    borderBottomWidth: 0.5,
    borderColor: '#C0C0C0'
  },
  listItemText: {
    textAlign: 'center'
  }
});
