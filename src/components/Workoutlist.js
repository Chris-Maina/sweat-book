import React from 'react';
import { FlatList, View, StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Button from 'apsl-react-native-button';
import { SText } from './index';

const renderInFooter = (handleAddPress) => (
  <View>
    <SText text="add some exercises" textStyles={styles.infoText} />
    <Button style={styles.plusButton} onPress={handleAddPress}>
      <Icon name="add" size={50} color="black" />
    </Button> 
  </View>
);

export const WorkoutList = props => {
  const { workoutList, handleAddPress } = props;
  return (
    <View>
      <FlatList
        data={workoutList}
        renderItem={({ item }) => (
          <View style={styles.listItem}>
            <SText style={styles.listItemText} text={item.name} />
          </View>
        )}
        ListFooterComponent={() => renderInFooter(handleAddPress)}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  listItem: {
    padding: 10,
    borderBottomWidth: 0.5,
    borderColor: '#C0C0C0'
  },
  listItemText: {
    textAlign: 'center',
    color: 'white'
  },
  infoText: {
    color: '#ffffff',
    fontSize: 28
  },
  plusButton: {
    borderStyle: null,
    borderWidth: 0,
    width: 50,
    alignSelf: 'center'
  }
});
