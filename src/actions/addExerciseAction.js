import {
  ADD_EXERCISE_REQUEST,
  ADD_EXERCISE_SUCCESS,
  ADD_EXERCISE_ERROR
} from './types';
import { post } from '../services/api';

export const addExerciseSuccess = (exercise) => ({
  type: ADD_EXERCISE_SUCCESS,
  payload: exercise
});

const addExerciseRequest = () => ({
  type: ADD_EXERCISE_REQUEST
});

const addExerciseError = (error) => ({
  type: ADD_EXERCISE_ERROR,
  payload: error
});

export const addExerciseToCurrentWorkout = (exercise) => {
  return async (dispatch) => {
    // dispatch(addExerciseRequest());
    try {
      const createdExercise = await post('current_workout', exercise);
      dispatch(addExerciseSuccess(createdExercise))
    } catch(err) {
      dispatch(addExerciseError(err));
    }
  }
}
