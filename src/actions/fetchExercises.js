import { 
  FETCH_CURRENT_WORKOUT_REQUEST,
  FETCH_CURRENT_WORKOUT_SUCCESS,
  FETCH_CURRENT_WORKOUT_ERROR
 } from './types';
 import { get } from '../services/api';

const fetchCurrentWorkoutRequest = () => ({
  type: FETCH_CURRENT_WORKOUT_REQUEST
});


const fetchCurrentWorkoutSuccess = (workouts) => ({
  type: FETCH_CURRENT_WORKOUT_SUCCESS,
  payload: workouts
});

const fetchCurrentWorkoutError = (error) => ({
  type: FETCH_CURRENT_WORKOUT_ERROR,
  payload: error
});

export const fetchExercisesCurrentWorkout = () => (
  async dispatch => {
    try {
      // dispatch(fetchCurrentWorkoutRequest());
      const workouts = await get('current_workout');
      dispatch(fetchCurrentWorkoutSuccess(workouts));
    } catch (error) {
      dispatch(fetchCurrentWorkoutError(error));
    }
  }
)