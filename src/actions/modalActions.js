import { SET_MODAL_VISIBILITY } from './types';

export const setModalVisibility = (visibility) => (
  {
    type: SET_MODAL_VISIBILITY,
    payload: visibility,
  }
);
