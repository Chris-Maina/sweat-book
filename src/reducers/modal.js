import initialState from './initialState';
import { SET_MODAL_VISIBILITY } from '../actions/types';

export const modalVisibility = (state=initialState.modalVisibility, {type, payload}) => {
  switch (type) {
    case SET_MODAL_VISIBILITY:
      return payload;
    default:
      return state;
  }
}
