import initialState from './initialState';
import { 
  ADD_EXERCISE_REQUEST
} from '../actions/types';

export const exercises = (state=initialState.exercises, {type, payload}) => {
  switch (type) {
    case ADD_EXERCISE_REQUEST:
      return [
        ...state,
        payload
      ]
    default:
      return state;
  }
}
