import { exercises } from '../constants';

const initialState = {
  exercises,
  modalVisibility: false,
  currentWorkout: {
    workout: [],
    error: {}
  }
}

export default initialState;
