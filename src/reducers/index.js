import { combineReducers } from 'redux';
import { exercises } from './exercises';
import { modalVisibility } from './modal';
import { currentWorkout } from './current_workout';

export default combineReducers({ 
  exercises,
  modalVisibility,
  currentWorkout
 });
