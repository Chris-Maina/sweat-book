import initialState from './initialState';
import { 
  ADD_EXERCISE_SUCCESS,
  ADD_EXERCISE_ERROR,
  FETCH_CURRENT_WORKOUT_SUCCESS,
  FETCH_CURRENT_WORKOUT_ERROR
} from '../actions/types';

export const currentWorkout = (state=initialState.currentWorkout, {type, payload}) => {
  switch (type) {
    case FETCH_CURRENT_WORKOUT_SUCCESS:
      return {
        ...state,
        workout: [...payload, ...state.workout]
      }
    case FETCH_CURRENT_WORKOUT_ERROR:
      return {
        ...state,
        error: payload
      }
    case ADD_EXERCISE_SUCCESS:
      return {
        ...state,
        workout: [payload, ...state.workout]
      }
    case ADD_EXERCISE_ERROR:
      return {
        ...state,
        error: payload
      }
    default:
      return state;
  }
}
