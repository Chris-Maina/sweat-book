import React, { Component } from 'react';
import { View, StyleSheet } from 'react-native';
import { connect } from 'react-redux';
import LinearGradient from 'react-native-linear-gradient';

import { SText, Topbar, SweatModal, WorkoutList } from '../components';

import { setModalVisibility } from '../actions/modalActions';
import { addExerciseToCurrentWorkout, fetchExercisesCurrentWorkout } from '../actions';

class _CurrentWorkout extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.props.fetchExercisesCurrentWorkout();
  }

  closeModal = () => {
    const { setModalVisibility } = this.props;
    // action to close a modal
    setModalVisibility(false);
  };
  handleAddPress = () => {
    const { setModalVisibility } = this.props;
    // action to open a modal
    setModalVisibility(true);
  };
  render() {
    const {
      modalVisibility,
      exercises,
      addExercise,
      currentWorkout
    } = this.props;
    return (
      <View style={styles.container}>
        <LinearGradient
          colors={['#52EDC7', '#5AC8FB']}
          style={styles.container}
        >
          <Topbar style={styles.headerContainer}>
            <SText text="Current Workout" textStyles={styles.headerText} />
          </Topbar>
          <View style={styles.infoContainer}>
            <WorkoutList 
              workoutList={currentWorkout}
              handleAddPress={this.handleAddPress}
            />
          </View>
        </LinearGradient>
        <SweatModal
          exercises={exercises}
          visibility={modalVisibility}
          addExercise={addExercise}
          closeModal={this.closeModal}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  headerContainer: {
    borderBottomWidth: 2,
    borderColor: '#ffffff',
    alignSelf: 'stretch'
  },
  headerText: {
    color: '#ffffff',
    textAlign: 'center'
  },
  infoContainer: {
    marginTop: 'auto',
    marginBottom: 'auto',
    marginLeft: 'auto',
    marginRight: 'auto'
  },
});

const mapStateToProps = state => {
  return {
    currentWorkout: state.currentWorkout.workout,
    modalVisibility: state.modalVisibility,
    exercises: state.exercises
  };
};

const mapActionsToProps = dispatch => ({
  setModalVisibility(visibility) {
    return dispatch(setModalVisibility(visibility));
  },
  addExercise(exercise) {
    return dispatch(addExerciseToCurrentWorkout(exercise));
  },
  fetchExercisesCurrentWorkout() {
    return dispatch(fetchExercisesCurrentWorkout());
  }
});

export const CurrentWorkout = connect(
  mapStateToProps,
  mapActionsToProps
)(_CurrentWorkout);
