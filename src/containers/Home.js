import React from 'react';
import { View, StyleSheet } from 'react-native';
import { SText, SButton } from '../components';
import LinearGradient from 'react-native-linear-gradient';

export const Home = props => {
  const { isStarted } = props;
  return (
    <LinearGradient 
      colors={['#6a1282', '#7d7fba']}
      style={styles.homeContainer}>
      <View style={styles.sweatTitleContainer}>
        <SText textStyles={styles.sweatTitle} text="Sweat Book" />
      </View>
      <View style={styles.infoContainer}>
        <SText textStyles={styles.workoutInfo} text="Your last workout" />
        <SText textStyles={styles.date} text="Saturday Jun 12th" />
      </View>
      <View style={styles.buttonContainer}>
        <SButton
          additionalTextStyles={styles.additionalTextStyles}
          buttonStyles={styles.startWorkoutBtn}
          buttonText="Start Workout"
          onPress={isStarted} />
      </View>
    </LinearGradient>
  );
};

const styles = StyleSheet.create({
  homeContainer: {
    flex: 1,
    alignItems: 'center',
  },
  sweatTitleContainer: {
    padding: 10,
  },
  sweatTitle: {
    fontSize: 36,
    fontWeight: 'bold',
    color: '#ffffff'
  },
  infoContainer: {
    marginTop: 'auto',
    marginBottom: 'auto'
  },
  workoutInfo: {
    textAlign: 'center',
    color: '#ffffff'
  },
  date: {
    fontSize: 28,
    fontWeight: '500',
    color: '#ffffff',
    textAlign: 'center',
  },
  buttonContainer: {
    marginBottom: 20,
  },
  startWorkoutBtn: {
    paddingLeft: 30,
    paddingRight: 30,
    borderColor: '#ffffff'
  },
  additionalTextStyles: {
    color: '#ffffff',
  }
});
