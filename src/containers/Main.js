import React from 'react';
import { StyleSheet, View } from 'react-native';
import ScrollableTabView from 'react-native-scrollable-tab-view';
import { SText } from '../components';
import { CurrentWorkout } from './CurrentWorkout'

export const Main = () => (
  <View style={styles.container}>
    <ScrollableTabView tabBarPosition="overlayBottom">
      <CurrentWorkout tabLabel='Workout' />
      <SText tabLabel='bye' text="bye" />
    </ScrollableTabView>
  </View>
);

const styles = StyleSheet.create({
  container: {
    flex: 1,
  }
});
