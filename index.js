/** @format */
import React from 'react';
import {AppRegistry} from 'react-native';
import App from './src';
import {name as SweatBook} from './app.json';
import { Provider } from 'react-redux';
import { store } from './src/store';

const Main = () => (
  <Provider store={store}>
    <App />
  </Provider>
);

AppRegistry.registerComponent(SweatBook, () => Main);
